.DEFAULT_GOAL := all
DESTDIR   ?= $(PWD)/_install
PREFIX    ?=
SHAREDIR  ?= /share/$(pkgname)

mk-clone   = git clone $1 $2
mk-cp      = cp $1 $2
mk-fetch   = wget -O - $1 | tar x --strip-components=1 --one-top-level=$2
mk-mkdir   = mkdir -p $1
mk-rm      = rm -rf $1
mk-untar   = tar xf $1 --strip-components 1 --one-top-level=$2
mk-wget    = T=`mktemp` && wget -q --show-progress -O $$$$T $1 && mv $$$$T $2 || (rm $$$$T && false)
mk-install = [ -n "$3" ] && install $1 $2 $3 || true

quiet_mk-clone   = $echo -e "CLONE:\t$2'; $(mk-clone)
quiet_mk-cp      = @echo -e 'CP:\t$1 $2'; $(mk-cp)
quiet_mk-fetch   = @echo -e 'FETCH:\t$2'; $(mk-fetch)
quiet_mk-mkdir   = @echo -e 'MKDIR:\t$1'; $(mk-mkdir)
quiet_mk-rm      = @echo -e 'RM:\t$1'; $(mk-rm)
quiet_mk-untar   = @echo -e 'UNTAR:\t$1'; $(mk-untar)
quiet_mk-wget    = @echo -e 'WGET:\t$2'; $(mk-wget)
quiet_mk-install = @echo -e 'INSTALL:\t$2; $(mk-install)

verbose_mk-clone   = @echo -e '$(mk-clone)'; $(mk-clone)
verbose_mk-cp      = @echo -e '$(mk-cp)'; $(mk-cp)
verbose_mk-fetch   = @echo -e '$(mk-fetch)'; $(mk-fetch)
verbose_mk-mkdir   = @echo -e '$(mk-mkdir)'; $(mk-mkdir)
verbose_mk-rm      = @echo -e '$(mk-rm)'; $(mk-rm)
verbose_mk-untar   = @echo -e '$(mk-untar)'; $(mk-untar)
verbose_mk-wget    = @echo -e '$(mk-wget)'; $(mk-wget)
verbose_mk-install = @echo -e '$(mk-install)'; $(mk-install)

# download and extract a tar file
# $1 - url
# $2 - dst
define mk-fetch-add =
$2:
	@$(call $(print)mk-fetch,$1,$$@)
endef

# download a file
# $1 - url
# $2 - dst
define mk-wget-add =
$2:
	@$(call $(print)mk-mkdir, `dirname $$@`)
	@$(call $(print)mk-wget,$1,$$@)
endef

# extract a file
# $1 - file
# $2 - dst
define mk-untar-add =
$2: $1
	@$(call $(print)mk-untar,$$<,$$@)
endef

# clone a git repository
# $1 - url
# $2 - dst
define mk-clone-add =
$2:
	@$(call $(print)mk-clone,$1,$$@)
endef
