mk-bpf-cc             = $$($1-toolchain)clang --target=bpf $$($1-cflags) -MT $$@ -MD -MF $$(@:.o=.d) -c -o $$@ $$<

quiet_mk-bpf-cc   = @echo -e "BPF:\t$$@"; $(mk-bpf-cc)

verbose_mk-bpf-cc = @echo -e "$(mk-bpf-cc)"; $(mk-bpf-cc)

define mk-bpf-def =
$1-sources:= $2
$1-srcdir := src/
$1-objdir := .o/
endef

define mk-bpf-add =
mk-bpf-obj-$1 := $$(patsubst %,$$($1-objdir)%.o,$$($1-sources))
mk-bpf-dep-$1 := $$(mk-bpf-obj-$1:.o=.d)

$$(mk-bpf-obj-$1): $$($1-objdir)%.o: $$($1-srcdir)%
	$$(shell mkdir -p `dirname $$@`)
	@$(call $(print)mk-bpf-cc,$1)

$1: $$(mk-bpf-obj-$1)
$1.clean:
	@$(call $(print)mk-rm,$$(mk-bpf-obj-$1) $$(mk-bpf-dep-$1))
endef

define mk-bpf-lst =
all:   $(foreach x,$1,$x)
clean: $(foreach x,$1,$x.clean)
endef

