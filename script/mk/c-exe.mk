# templated rules to build C and assembly programs, check the Makefile for usage


# TODO; change between `gcc` and `clang` (including cov)
# gcc/gcov has few options to specify the output directory/file.

mk-c-exe-as            = $$($1-toolchain)gcc $$($1-asflags) -MT $$@ -MMD -MP -MF $$(@:.o=.d) -c -o $$@ $$<
mk-c-exe-cc            = $$($1-toolchain)gcc $$($1-cflags) -MT $$@ -MMD -MP -MF $$(@:.o=.d) -c -o $$@ $$<
mk-c-exe-ld            = $$($1-toolchain)gcc $$($1-ldlibs) -o $$@ $$^
mk-c-exe-run           = $1
mk-c-exe-strip         = $$($1-toolchain)objcopy --add-gnu-debuglink=$$@ $$< $$@; \
                         $$($1-toolchain)strip --strip-debug --strip-unneeded $$@;
mk-c-exe-cflags        = -fsanitize=undefined -pedantic -fstrict-aliasing -Wstrict-aliasing=3

quiet_mk-c-exe-as      = @echo -e "AS:\t$$@"; $(mk-c-exe-as)
quiet_mk-c-exe-cc      = @echo -e "CC:\t$$@"; $(mk-c-exe-cc)
quiet_mk-c-exe-ld      = @echo -e "LD:\t$$@"; $(mk-c-exe-ld)
quiet_mk-c-exe-run     = @echo -e "RUN:\t$1"; $(mk-c-exe-run)
quiet_mk-c-exe-strip   = @echo -e "STRIP:\t$$@"; $(mk-c-exe-strip)

verbose_mk-c-exe-as    = @echo -e "$(mk-c-exe-as)"; $(mk-c-exe-as)
verbose_mk-c-exe-cc    = @echo -e "$(mk-c-exe-cc)"; $(mk-c-exe-cc)
verbose_mk-c-exe-ld    = @echo -e "$(mk-c-exe-ld)"; $(mk-c-exe-ld)
verbose_mk-c-exe-run   = @echo -e "$(mk-c-exe-run)"; $(mk-c-exe-run)
verbose_mk-c-exe-strip = @echo -e "$(mk-c-exe-strip)"; $(mk-c-exe-strip)

# exe --------------------------------------------------------------------------
# instantiate default variables used by mk-c-exe-add.
# $1 - name used in mk-c-exe-add and the executable to be created.
# $2 - list of .c and .S source files separated by spaces
# $3 - list of .h header files to be installed with the binary separated by spaces
# $4 - cflags to build and link the program
define mk-c-exe-def =
$1-exe    := bin/$1
$1-sources:= $2
$1-headers:= $3
$1-srcdir := src/
$1-hdrdir := src/
$1-objdir := .o/
$1-cflags := $4
$1-ldlibs := $$($1-cflags)
endef

# generate rules to create a C executable.
# $1 - name used in mk-c-exe-add.
define mk-c-exe-add =
mk-c-exe-obj-c-$1   := $$(patsubst %,$$($1-objdir)%.o,$$(filter %.c,$$($1-sources)))
mk-c-exe-obj-s-$1   := $$(patsubst %,$$($1-objdir)%.o,$$(filter %.S,$$($1-sources)))
mk-c-exe-obj-$1     := $$(mk-c-exe-obj-c-$1) $$(mk-c-exe-obj-s-$1)
mk-c-exe-dep-$1     := $$(mk-c-exe-obj-c-$1:.o=.d) $$(mk-c-exe-obj-s-$1:.o=.d)
$1-dbg              := $($1-objdir)$($1-exe)
$1-help-str         += build clean help

$$($1-dbg): $$(mk-c-exe-obj-$1)
	@mkdir -p $$(@D)
	@$(call $(print)mk-c-exe-ld,$1)
$$($1-exe): $$($1-dbg)
	@mkdir -p $$(@D)
	@$(call $(print)mk-c-exe-strip,$1)

$$(mk-c-exe-obj-c-$1): $$($1-objdir)%.o: $$($1-srcdir)%
	@mkdir -p $$(@D)
	@$(call $(print)mk-c-exe-cc,$1)
$$(mk-c-exe-obj-s-$1): $$($1-objdir)%.o: $$($1-srcdir)%
	@mkdir -p $$(@D)
	@$(call $(print)mk-c-exe-as,$1)

$1.build: $$($1-exe)
$1.clean:
	@$(call $(print)mk-rm,$$($1-dbg) $$($1-exe) $$(mk-c-exe-obj-$1) $$(mk-c-exe-dep-$1))
$1.help:
	@echo "  $1: $$($1-help-str)"

-include $$(mk-c-exe-dep-$1)
endef

# install the stripped binary and headers specified in mk-c-exe-add to DESTDIR and PREFIX.
# $1 - name used in mk-c-exe-add.
define mk-c-exe-install =
$1-help-str += install
$1.install: $1.build
	@$(call $(print)mk-install,-m 755 -Dt,$$(DESTDIR)$(PREFIX)/bin,$($1-exe))
	@$(call $(print)mk-install,-m 660 -Dt,$$(DESTDIR)$(PREFIX)/include,$(addprefix $($1-hdrdir),$($1-headers)))
install:   $(foreach x,$1,$x.install)
endef

# add entries to 'global' rules
# $1 - name used in mk-c-exe-add.
define mk-c-exe-lst =
all:     $(foreach x,$1,$x.build)
clean:   $(foreach x,$1,$x.clean)
install: $(foreach x,$1,$x.install)
help:    $(foreach x,$1,$x.help)
endef

# test the program by running it.
# $1 - name used in mk-c-exe-add.
define mk-c-exe-test =
$1-help-str += test
$1.test: $$($1-exe)
	@$(call $(print)mk-c-exe-run,$$<,$1)
test:   $(foreach x,$1,$x.test)
endef

# lib --------------------------------------------------------------------------
define mk-c-lib-def =
$1-exe    := bin/$1
$1-sources:= $2
$1-headers:= $3
$1-srcdir := src/
$1-hdrdir := src/
$1-objdir := .o/
$1-cflags := -ggdb -O2 -fPIC -shared $4
$1-ldlibs := $$($1-cflags)
endef

define mk-c-lib-add =
$(eval $(call mk-c-exe-add,$1))
endef

define mk-c-lib-lst =
$(eval $(call mk-c-exe-lst,$1))
endef

define mk-c-lib-install =
$1-help-str += install
$1.install: $1.build
	@$(call $(print)mk-install,-m 755 -Dt,$$(DESTDIR)$(PREFIX)/lib,$($1-exe))
	@$(call $(print)mk-install,-m 660 -Dt,$$(DESTDIR)$(PREFIX)/include,$(addprefix $($1-hdrdir),$($1-headers)))
endef

