mk-glsl-cc         = $$($1-toolchain)glslc $$($1-cflags) -MT $$@ -MD -MF $$(@:.o=.d) -c -o $$@ $$<

quiet_mk-glsl-cc   = @echo -e "GLSLC:\t$$@"; $(mk-glsl-cc)

verbose_mk-glsl-cc = @echo -e "$(mk-glsl-cc)"; $(mk-glsl-cc)

define mk-glsl-def =
$1-sources:= $2
$1-srcdir := src/
$1-objdir := .o/
endef

define mk-glsl-add =
mk-glsl-obj-$1 := $$(patsubst %,$$($1-objdir)%.o,$$($1-sources))
mk-glsl-dep-$1 := $$(mk-glsl-obj-$1:.o=.d)

$$(mk-glsl-obj-$1): $$($1-objdir)%.o: $$($1-srcdir)%
	$$(shell mkdir -p `dirname $$@`)
	@$(call $(print)mk-glsl-cc,$1)

$1: $$(mk-glsl-obj-$1)
$1.clean:
	@$(call $(print)mk-rm,$$(mk-glsl-obj-$1) $$(mk-glsl-dep-$1))
endef

define mk-glsl-lst =
all:   $(foreach x,$1,$x)
clean: $(foreach x,$1,$x.clean)
endef

