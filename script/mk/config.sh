if [ "$verbose" = "yes" ]; then
	print=verbose_
elif [ "$quiet" = "yes" ]; then
	print=quiet_
else
	print=
fi

cat <<- EOF
	pkgname=pkg
	major=0
	minor=0
	patch=0
	label=
	print=$print
	EOF
