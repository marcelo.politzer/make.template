mk-kmod-build = $$(MAKE) -sC $1 M=$2 INSTALL_MOD_PATH=$3 INSTALL_MOD_STRIP=1 modules modules_install
mk-kmod-clean = [ -d "$1" ] && $$(MAKE) -sC $1 M=$2 clean || true

quiet_mk-kmod-build = @echo -e "MAKE\t$$($(1)-mpath)"; $(mk-kmod-build)
quiet_mk-kmod-clean = @echo -e "MAKE\t$$@"; $(mk-kmod-clean)

verbose_mk-kmod-build = @echo -e "$(mk-kmod-build)"; $(mk-kmod-build)
verbose_mk-kmod-clean = @echo -e "$(mk-kmod-clean)"; $(mk-kmod-clean)

define mk-kmod-def =
$1-mpath := $2
$1-kpath := $3
$1-ipath := $4
endef

define mk-kmod-add =
$1: $$($1-kpath)
	@$(call $(print)mk-kmod-build \
		,$$(abspath $$($1-kpath)) \
		,$$(abspath $$($1-mpath)) \
		,$$(abspath $$($1-ipath)))
$1.clean:
	@$(call $(print)mk-kmod-clean \
		,$$(abspath $$($1-kpath)) \
		,$$(abspath $$($1-mpath)))
.PHONY: $1 $1.clean
endef

define mk-kmod-lst =
all:   $(foreach x,$1,$x)
clean: $(foreach x,$1,$x.clean)
endef
