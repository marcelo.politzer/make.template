#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in  vec3 fcolor;
layout(location = 0) out vec4 o;

void main() {
    o = vec4(1.0, 0.0, 0.0, 1.0);
}
