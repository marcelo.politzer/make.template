include script/mk/include.mk
include script/mk/c-exe.mk
include script/mk/kmod.mk
include script/mk/glsl.mk
include script/mk/bpf.mk
-include .config

ifeq ($(OS),Windows_NT)
# windows
else
OS := $(shell [ -f /etc/os-release ] && echo linux || echo macos)

ifeq ($(OS),linux)
ID := $(shell awk -F= '/^ID=/ {print $$2}' /etc/os-release)
# linux
endif

ifeq ($(OS),macos)
# macos
endif

endif

defconfig:
	@script/mk/config.sh > .config
version:
	@echo $(major).$(minor).$(patch)$(label)
env:
	@echo "OS=$(OS)"
	@echo "ID=$(ID)"
generic.help:
	@echo "generic rules (* default):"
	@echo "* all            - default target"
	@echo "  defconfig      - create a default configuration"
	@echo "  version        - print version"
	@echo "  help           - display this list"
	@echo "  install        - specify DESTDIR=\$$PWD/_install and PREFIX=/usr for installation"
	@echo "  clean          - remove generated files"
	@echo ""
	@echo "object rules:"
help: generic.help

## -----------------------------------------------------------------------------
## build a C program
$(eval $(call mk-c-exe-def,main,main.c,,$(mk-c-exe-cflags)))
$(eval $(call mk-c-exe-add,main))
$(eval $(call mk-c-exe-lst,main))
$(eval $(call mk-c-exe-install,main))
$(eval $(call mk-c-exe-test,main))

## build a C library
$(eval $(call mk-c-lib-def,libfoo.so,foo.c,foo.h,$(mk-c-exe-cflags)))
$(eval $(call mk-c-lib-add,libfoo.so))
$(eval $(call mk-c-lib-lst,libfoo.so))
$(eval $(call mk-c-lib-install,libfoo.so))

## -----------------------------------------------------------------------------
## build a linux kernel module
dep/linux:
	@$(call $(print)mk-clone,https://github.com/torvalds/linux.git --depth=1,$@)
	@make -C $@ defconfig prepare modules_prepare

$(eval $(call mk-kmod-def,kmod,src/kmod,dep/linux,/tmp/x))
$(eval $(call mk-kmod-add,kmod))
$(eval $(call mk-kmod-lst,kmod))

## -----------------------------------------------------------------------------
## build glsl objects
$(eval $(call mk-glsl-def,red,glsl/red.frag glsl/red.vert))
$(eval $(call mk-glsl-add,red))
$(eval $(call mk-glsl-lst,red))

## -----------------------------------------------------------------------------
## build bpf objects
$(eval $(call mk-bpf-def,samples,bpf/pass.c bpf/drop.c))
$(eval $(call mk-bpf-add,samples))
$(eval $(call mk-bpf-lst,samples))
